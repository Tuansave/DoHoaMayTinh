#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	
	
	
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
	SDL_RenderDrawPoint(ren, x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, y + xc, x + yc);
	SDL_RenderDrawPoint(ren, -y + xc, x + yc);
	SDL_RenderDrawPoint(ren, y + xc, -x + yc);
	SDL_RenderDrawPoint(ren, -y + xc, -x + yc);

   
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0; int y = R;
	int p = 3 - 2 * R;
	while (x <= y)
	{
		Draw8Points(xc, yc, x, y, ren);
		if (p<0) p = p + 4 * x + 6;
		else
		{
			p = p + 4 * (x - y) + 10;
			y = y - 1;
		}
		x = x + 1;
	}
	
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0; int y = R;
	int f = 1 - R;
	Draw8Points(xc, yc, x, y,ren);
	while (x < y)
	{
		if (f < 0) f += (x << 1) + 3;
		else
		{
			y--;
			f += ((x - y) << 1) + 5;
		}
		x++;
		Draw8Points(xc, yc, x, y, ren);
	}
}
