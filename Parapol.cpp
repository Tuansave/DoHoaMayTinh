#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
}

void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	
	int x = 0;
	int y = 0;
	int p = 1 - A;
	while (x < A)
	{
		Draw2Points(xc, yc, x, y, ren);
		if (p <= 0) p = p + 2 * x + 3;
		else
		{
			y++;
			p = p + 2 * x + 3 - 2 * A;
		}
		x++;
	}

	int x1 = A;
	int y1 = A / 2;

	int p1 = 2 * A - 1;
	int w, h;
	int res = SDL_GetRendererOutputSize(ren, &w, &h);
	
	while (y1 < h)
	{
		Draw2Points(xc, yc, x1, y1, ren);
		if (p1 <= 0) p1 = p1 + 4 * A;
		else
		{
			x1++;
			p1 = p1 + 4 * A - 4 * x - 4;
		}
		y1++;
	}
}
void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0;
	int y = 0;
	int p = 1 - A;
	while (x < A)
	{
		Draw2Points(xc, yc, -x, -y, ren);
		if (p <= 0) p = p + 2 * x + 3;
		else
		{
			y++;
			p = p + 2 * x + 3 - 2 * A;
		}
		x++;
	}

	int x1 = A;
	int y1 = A / 2;

	int p1 = 2 * A - 1;
	int w, h;
	int res = SDL_GetRendererOutputSize(ren, &w, &h);

	while (y1 < h)
	{
		Draw2Points(xc, yc, -x1, -y1, ren);
		if (p1 <= 0) p1 = p1 + 4 * A;
		else
		{
			x1++;
			p1 = p1 + 4 * A - 4 * x - 4;
		}
		y1++;
	}
}